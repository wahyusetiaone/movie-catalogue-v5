package in.setone.moviecatalogue5.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "movies_fav")
public class MoviesModel {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    @SerializedName("id")
    @Expose
    protected int id;

    @ColumnInfo(name = "popularity")
    @SerializedName("popularity")
    @Expose
    protected double popularity;

    @ColumnInfo(name = "vote_count")
    @SerializedName("vote_count")
    @Expose
    protected int vote_count;

    @ColumnInfo(name = "video")
    @SerializedName("video")
    @Expose
    protected boolean video;

    @ColumnInfo(name = "poster_path")
    @SerializedName("poster_path")
    @Expose
    protected String poster_path;

    @ColumnInfo(name = "adult")
    @SerializedName("adult")
    @Expose
    protected boolean adult;

    @ColumnInfo(name = "backdrop_path")
    @SerializedName("backdrop_path")
    @Expose
    protected String backdrop_path;

    @ColumnInfo(name = "original_language")
    @SerializedName("original_language")
    @Expose
    protected String original_language;

    @ColumnInfo(name = "original_title")
    @SerializedName("original_title")
    @Expose
    protected String original_title;

    @ColumnInfo(name = "title")
    @SerializedName("title")
    @Expose
    protected String title;

    @ColumnInfo(name = "vote_average")
    @SerializedName("vote_average")
    @Expose
    protected double vote_average;

    @ColumnInfo(name = "overview")
    @SerializedName("overview")
    @Expose
    protected String overview;

    @ColumnInfo(name = "release_date")
    @SerializedName("release_date")
    @Expose
    protected String release_date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPopularity() {
        return popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    public int getVote_count() {
        return vote_count;
    }

    public void setVote_count(int vote_count) {
        this.vote_count = vote_count;
    }

    public boolean isVideo() {
        return video;
    }

    public void setVideo(boolean video) {
        this.video = video;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public boolean isAdult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public void setBackdrop_path(String backdrop_path) {
        this.backdrop_path = backdrop_path;
    }

    public String getOriginal_language() {
        return original_language;
    }

    public void setOriginal_language(String original_language) {
        this.original_language = original_language;
    }

    public String getOriginal_title() {
        return original_title;
    }

    public void setOriginal_title(String original_title) {
        this.original_title = original_title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getVote_average() {
        return vote_average;
    }

    public void setVote_average(double vote_average) {
        this.vote_average = vote_average;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }
}
