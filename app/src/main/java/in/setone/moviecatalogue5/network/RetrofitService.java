package in.setone.moviecatalogue5.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitService {

    private static String URL_API_THEMOVIEDB = "https://api.themoviedb.org/3/";
    private static String URL_API_IMAGES = "https://image.tmdb.org/t/p/w500/";

    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(URL_API_THEMOVIEDB)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    public static <S> S createService(Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }

}
