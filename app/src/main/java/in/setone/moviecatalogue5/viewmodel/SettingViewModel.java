package in.setone.moviecatalogue5.viewmodel;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;
import in.setone.moviecatalogue5.model.AppSettingModel;
import in.setone.moviecatalogue5.repository.SettingRepository;

public class SettingViewModel extends ViewModel {
    private LiveData<AppSettingModel> appSettingModel;
    private SettingRepository settingRepository;

    public void init(Activity activity) {
            if (appSettingModel != null) {
                return;
            }
        settingRepository = SettingRepository.getInstance(activity);

        //call from database
        appSettingModel = settingRepository.read();

    }

    public LiveData<AppSettingModel> getAppSetting() {
        return appSettingModel;
    }

    public final void update(AppSettingModel appSettingModel, Activity activity) {
        settingRepository = SettingRepository.getInstance(activity);
        try {
            settingRepository.update(appSettingModel);
        } catch (Exception ee) {
            Log.wtf("ERROR UPDATE SETTING", ee.toString());
        }
    }


}
