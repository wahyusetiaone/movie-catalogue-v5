package in.setone.moviecatalogue5.room;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomWarnings;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import in.setone.moviecatalogue5.model.MoviesModel;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {MoviesModel.class}, version = 1, exportSchema = false)
public abstract class MoviesRoom extends RoomDatabase {

    public abstract MoviesDao moviesDao();

    // marking the instance as volatile to ensure atomic access to the variable
    private static volatile MoviesRoom INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    @SuppressWarnings(RoomWarnings.CANNOT_CREATE_VERIFICATION_DATABASE)
    public static MoviesRoom getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (MoviesRoom.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            MoviesRoom.class, "moviecatalog_db")
//                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    /**
     * Override the onOpen method to populate the database.
     * For this sample, we clear the database every time it is created or opened.
     *
     * If you want to populate the database only when the database is created for the 1st time,
     * override RoomDatabase.Callback()#onCreate
     */
    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db){
            super.onCreate(db);

            Log.wtf("DB PATH",db.getPath());

        }

        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);

            // If you want to keep data through app restarts,
            // comment out the following block
            databaseWriteExecutor.execute(() -> {
                // Populate the database in the background.
                // If you want to start with more words, just add them.
                MoviesDao dao = INSTANCE.moviesDao();
                dao.deleteAll();

//                MoviesTableModel word = new MoviesTableModel(10001);
//                dao.insert(word);
//                word = new MoviesTableModel(10002);
//                dao.insert(word);
//                word = new MoviesTableModel(10003);
//                dao.insert(word);
            });
        }
    };
//    private static MoviesRoom INSTANCE;
//
//        public static MoviesRoom getDatabase(final Context context) {
//        if (INSTANCE == null) {
//            synchronized (MoviesRoom.class) {
//                if (INSTANCE == null) {
//                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
//                            MoviesRoom.class, "moviecatalog_db")
////                            .fallbackToDestructiveMigration()
//                            //dummy input
//                            .addCallback(sRoomDatabaseCallback)
//                            .build();
//                }
//            }
//        }
//        return INSTANCE;
//    }
//
//    private static RoomDatabase.Callback sRoomDatabaseCallback =
//            new RoomDatabase.Callback(){
//
//                @Override
//                public void onOpen (@NonNull SupportSQLiteDatabase db){
//                    super.onOpen(db);
//                    new MoviesPopulate(INSTANCE).execute();
//                }
//            };
}
