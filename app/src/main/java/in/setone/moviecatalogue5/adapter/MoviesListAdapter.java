package in.setone.moviecatalogue5.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import in.setone.moviecatalogue5.R;
import in.setone.moviecatalogue5.activity.DetailsMovieActivity;
import in.setone.moviecatalogue5.model.MoviesModel;
import in.setone.moviecatalogue5.parcelable.Movies;
import in.setone.moviecatalogue5.viewmodel.MoviesViewModel;
import in.setone.moviecatalogue5.widget.StackWidgetService;

import java.util.List;

public class MoviesListAdapter extends RecyclerView.Adapter<MoviesListAdapter.ListViewHolder> {

    private List<Movies> arrayMovies;
    private List<Movies> dbMovies;
    private Context context;
    private Activity activity;
    private boolean fav = false;

    public static String KEY_OF_MOVIE = "DATA_MOVIE";
    public static String KEY_OF_MOVIE_FAVORITE = "FAV_MOVIE";

    public MoviesListAdapter(){}

    public MoviesListAdapter(List<Movies> moviesArrayList, Activity activity, List<Movies> db){
        this.arrayMovies = moviesArrayList;
        this.activity = activity;
        this.dbMovies = db;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row_movies, viewGroup, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder listViewHolder, int i) {
        final Movies movies = arrayMovies.get(i);
        boolean fav;

        String url_image = "https://image.tmdb.org/t/p/w185" + movies.getPoster_path();
        Picasso.get().load(url_image).into(listViewHolder.imgPoster);

        listViewHolder.tvTitle.setText(movies.getTitle());

        listViewHolder.tvDetail.setText(movies.getOverview());

        if (movies.getOverview().equals("")){
            listViewHolder.tvDetail.setText(R.string.no_detalis);
        }

        fav = setFavButton(listViewHolder.btnFav,dbMovies,movies);

        listViewHolder.cVMovies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.wtf("FAV",String.valueOf(fav));
                Intent i = new Intent(context, DetailsMovieActivity.class);
                i.putExtra(KEY_OF_MOVIE, movies);
                i.putExtra(KEY_OF_MOVIE_FAVORITE,fav);
                context.startActivity(i);
            }
        });

        listViewHolder.btnFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                favImage(v,movies);
            }
        });

    }

    @SuppressLint("Assert")
    private void favImage(View view, Movies movies) {

        MoviesViewModel viewModel = new MoviesViewModel();

        ImageView imageView = (ImageView) view;
        assert(R.id.btnFavMovies == imageView.getId());

        Integer integer = (Integer) imageView.getTag();
        integer = integer == null ? 0 : integer;

        switch(integer) {
            case R.drawable.ic_star_black_24dp:
                viewModel.delete(movies,activity);
                break;
            case R.drawable.ic_star_border_black_24dp:
                viewModel.insert(movies,activity);
                break;
        }
        StackWidgetService.updateWidget(context,activity.getApplication());
    }

    private boolean setFavButton(ImageView imageView, List<Movies> dbMovies, Movies movies) {

        imageView.setImageResource(R.drawable.ic_star_border_black_24dp);
        imageView.setTag(R.drawable.ic_star_border_black_24dp);

        for (MoviesModel model: dbMovies) {
            if (movies.getId() == model.getId()){
                imageView.setImageResource(R.drawable.ic_star_black_24dp);
                imageView.setTag(R.drawable.ic_star_black_24dp);
                return true;
            }
        }
        if(fav){
            imageView.setImageResource(R.drawable.ic_star_black_24dp);
            imageView.setTag(R.drawable.ic_star_black_24dp);
        }
        return false;
    }


    @Override
    public int getItemCount() {
        return arrayMovies.size();
    }

    public void addContext(Context context, boolean b) {
        this.context = context;
        this.fav = b;
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {

        ImageView imgPoster, btnFav;
        TextView tvTitle,tvDetail;
        CardView cVMovies;

        public ListViewHolder(@NonNull View itemView) {
            super(itemView);

            btnFav = itemView.findViewById(R.id.btnFavMovies);
            imgPoster = itemView.findViewById(R.id.imgPosterMovie);
            tvTitle = itemView.findViewById(R.id.tvTitleMovie);
            tvDetail = itemView.findViewById(R.id.tvDetailMovie);
            cVMovies = itemView.findViewById(R.id.cVMovies);
        }
    }
}
