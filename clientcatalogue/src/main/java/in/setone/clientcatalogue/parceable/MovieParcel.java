package in.setone.clientcatalogue.parceable;

import android.os.Parcel;
import android.os.Parcelable;
import in.setone.clientcatalogue.model.MovieModel;

public class MovieParcel extends MovieModel implements Parcelable {

    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeDouble(this.popularity);
        dest.writeInt(this.vote_count);
        dest.writeByte((byte) (this.video ? 1: 0));
        dest.writeString(this.poster_path);
        dest.writeByte((byte) (this.adult ? 1: 0));
        dest.writeString(this.backdrop_path);
        dest.writeString(this.original_language);
        dest.writeString(this.original_title);
        dest.writeString(this.title);
        dest.writeDouble(this.vote_average);
        dest.writeString(this.overview);
        dest.writeString(this.release_date);

    }

    public MovieParcel(){
    }

    public MovieParcel(int id, double popularity, int vote_count, boolean video, String poster_path, boolean adult, String backdrop_path, String original_language, String original_title, String title, double vote_average, String overview, String release_date) {
        super(id,popularity,vote_count,video,poster_path,adult,backdrop_path,original_language,original_title,title,vote_average,overview,release_date);
    }

    private MovieParcel(Parcel in) {
        this.id = in.readInt();
        this.popularity = in.readDouble();
        this.vote_count = in.readInt();
        this.video = in.readByte() != 0;
        this.poster_path = in.readString();
        this.adult = in.readByte() != 0;;
        this.backdrop_path = in.readString();
        this.original_language = in.readString();
        this.original_title = in.readString();
        this.title = in.readString();
        this.vote_average = in.readDouble();
        this.overview = in.readString();
        this.release_date = in.readString();
    }
    public static final Creator<MovieParcel> CREATOR = new Creator<MovieParcel>() {
        @Override
        public MovieParcel createFromParcel(Parcel source) {
            return new MovieParcel(source);
        }
        @Override
        public MovieParcel[] newArray(int size) {
            return new MovieParcel[size];
        }
    };

}
