package in.setone.moviecatalogue5.repository;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import in.setone.moviecatalogue5.activity.ErrorActivity;
import in.setone.moviecatalogue5.model.MoviesModel;
import in.setone.moviecatalogue5.model.MoviesResponseModel;
import in.setone.moviecatalogue5.network.RetrofitService;
import in.setone.moviecatalogue5.network.ServicesAPI;
import in.setone.moviecatalogue5.parcelable.Movies;
import in.setone.moviecatalogue5.room.AppDatabase;
import in.setone.moviecatalogue5.room.MoviesDao;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;

public class MoviesRepository {
    private static MoviesRepository moviesRepository;

    private MoviesDao moviesDao;

    public static MoviesRepository getInstance(Activity activity) {
        if (moviesRepository == null) {
            moviesRepository = new MoviesRepository(activity);
        }
        return moviesRepository;
    }

    private ServicesAPI servicesAPI;
    private AppDatabase appDatabase;

    public MoviesRepository(Activity activity) {
        servicesAPI = RetrofitService.createService(ServicesAPI.class);
        appDatabase = AppDatabase.getDatabase(activity);
        appDatabase.getOpenHelper().getWritableDatabase();
        moviesDao = appDatabase.moviesDao();
    }

    public MutableLiveData<MoviesResponseModel> getMovies(String apiKey, String language, final Activity activity) {
        final MutableLiveData<MoviesResponseModel> modelMutableLiveData = new MutableLiveData<>();
        servicesAPI.getDataMovies(apiKey, language).enqueue(new Callback<MoviesResponseModel>() {
            @Override
            public void onResponse(Call<MoviesResponseModel> call,
                                   Response<MoviesResponseModel> response) {
                if (response.isSuccessful()) {
                    modelMutableLiveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<MoviesResponseModel> call, Throwable t) {
                Log.wtf("ERROR", t.toString());
                Intent intent = new Intent(activity, ErrorActivity.class);
                activity.finish();
                activity.startActivity(intent);
            }
        });
        return modelMutableLiveData;
    }

    public LiveData<List<Movies>> read() {

        return moviesDao.getResults();
    }

    public void delete(MoviesModel model){
        new deleteAsyncTask(moviesDao).execute(model);
    }

    public void insert(MoviesModel tableModel) {
        new insertAsyncTask(moviesDao).execute(tableModel);
    }

    private static class insertAsyncTask extends AsyncTask<MoviesModel, Void, Void> {

        private MoviesDao mAsyncTaskDao;

        insertAsyncTask(MoviesDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final MoviesModel... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private class deleteAsyncTask extends AsyncTask<MoviesModel, Void, Void> {
        private MoviesDao mAsyncTaskDao;

        deleteAsyncTask(MoviesDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final MoviesModel... params) {
            mAsyncTaskDao.delete(params[0]);
            return null;
        }
    }

    public MutableLiveData<MoviesResponseModel> searchMovie(String apiKey, String language, String query, final Activity activity) {
        final MutableLiveData<MoviesResponseModel> modelMutableLiveData = new MutableLiveData<>();
        servicesAPI.searchMovie(apiKey, language, query).enqueue(new Callback<MoviesResponseModel>() {
            @Override
            public void onResponse(Call<MoviesResponseModel> call,
                                   Response<MoviesResponseModel> response) {
                if (response.isSuccessful()) {
                    modelMutableLiveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<MoviesResponseModel> call, Throwable t) {
                Log.wtf("ERROR", t.toString());
                Intent intent = new Intent(activity, ErrorActivity.class);
                activity.finish();
                activity.startActivity(intent);
            }
        });
        return modelMutableLiveData;
    }
}
