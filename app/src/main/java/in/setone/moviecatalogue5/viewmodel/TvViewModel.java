package in.setone.moviecatalogue5.viewmodel;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;
import in.setone.moviecatalogue5.BuildConfig;
import in.setone.moviecatalogue5.model.TvModel;
import in.setone.moviecatalogue5.model.TvResponseModel;
import in.setone.moviecatalogue5.parcelable.TvSeries;
import in.setone.moviecatalogue5.repository.TvRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

//TODO: Learn DataBinding

public class TvViewModel extends ViewModel {

    private MutableLiveData<TvResponseModel> mutableLiveData;
    private LiveData<List<TvSeries>> liveData;
    private TvRepository tvRepository;
    private final String API_KEY = BuildConfig.TMDB_API_KEY;
    private final String EN_LANGUANGE = "en-US";
    private final String ID_LANGUANGE = "id-ID";

    public void init(Activity activity, boolean reload) {
        if (!reload){
            if (mutableLiveData != null) {
                return;
            }
        }
        tvRepository = TvRepository.getInstance(activity);

        String lang = Locale.getDefault().getDisplayLanguage();

        if (lang.equals("Bahasa Indonesia")) {
            mutableLiveData = tvRepository.getTv(API_KEY, ID_LANGUANGE, activity);
        } else {
            mutableLiveData = tvRepository.getTv(API_KEY, EN_LANGUANGE, activity);
        }

        //call from database
        liveData = tvRepository.read();

    }

    public LiveData<List<TvSeries>> getTvDatabase() {
        return liveData;
    }

    public LiveData<TvResponseModel> getTvApi() {
        return mutableLiveData;
    }

    public final void clear(ArrayList<TvSeries> listMovies) {
        listMovies.clear();
        mutableLiveData.postValue(null);
    }

    public final void insert(TvModel movie, Activity activity) {
        tvRepository = TvRepository.getInstance(activity);
        try {
            tvRepository.insert(movie);
        } catch (Exception ee) {
            Log.wtf("ERROR INSERT THE MOVIE", ee.toString());
        }
    }

    public final void delete(TvModel movie, Activity activity) {
        tvRepository = TvRepository.getInstance(activity);
        try {
            tvRepository.delete(movie);
        } catch (Exception ee) {
            Log.wtf("ERROR DELETE THE MOVIE", ee.toString());
        }
    }

    public LiveData<TvResponseModel> searchTvApi(String query, Activity activity) {
        tvRepository = TvRepository.getInstance(activity);

        String lang = Locale.getDefault().getDisplayLanguage();

        if (lang.equals("Bahasa Indonesia")) {
            mutableLiveData = tvRepository.searchTv(API_KEY, ID_LANGUANGE, query, activity);
        } else {
            mutableLiveData = tvRepository.searchTv(API_KEY, EN_LANGUANGE, query, activity);
        }
        return mutableLiveData;
    }
}
