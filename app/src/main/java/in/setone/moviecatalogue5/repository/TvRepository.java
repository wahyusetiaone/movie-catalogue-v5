package in.setone.moviecatalogue5.repository;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import in.setone.moviecatalogue5.activity.ErrorActivity;
import in.setone.moviecatalogue5.model.TvModel;
import in.setone.moviecatalogue5.model.TvResponseModel;
import in.setone.moviecatalogue5.network.RetrofitService;
import in.setone.moviecatalogue5.network.ServicesAPI;
import in.setone.moviecatalogue5.parcelable.TvSeries;
import in.setone.moviecatalogue5.room.AppDatabase;
import in.setone.moviecatalogue5.room.TvDao;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;

public class TvRepository {
    private static TvRepository tvRepository;

    private TvDao tvDao;

    public static TvRepository getInstance(Activity activity){
        if (tvRepository == null){
            tvRepository = new TvRepository(activity);
        }
        return tvRepository;
    }

    private ServicesAPI servicesAPI;
    private AppDatabase appDatabase;

    public TvRepository(Activity activity){
        servicesAPI = RetrofitService.createService(ServicesAPI.class);
        appDatabase = AppDatabase.getDatabase(activity);
        appDatabase.getOpenHelper().getWritableDatabase();
        tvDao = appDatabase.tvDao();
    }

    public MutableLiveData<TvResponseModel> getTv(String apiKey, String language, final Activity activity){
        final MutableLiveData<TvResponseModel> modelMutableLiveData = new MutableLiveData<>();
        servicesAPI.getDataTv(apiKey, language).enqueue(new Callback<TvResponseModel>() {
            @Override
            public void onResponse(Call<TvResponseModel> call,
                                   Response<TvResponseModel> response) {
                if (response.isSuccessful()){
                    modelMutableLiveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<TvResponseModel> call, Throwable t) {
                Log.wtf("ERROR", t.toString());
                Intent intent = new Intent(activity, ErrorActivity.class);
                activity.finish();
                activity.startActivity(intent);
            }
        });
        return modelMutableLiveData;
    }

    public LiveData<List<TvSeries>> read() {

        return tvDao.getResults();
    }

    public void delete(TvModel model){
        new deleteAsyncTask(tvDao).execute(model);
    }

    public void insert(TvModel tableModel) {
        new insertAsyncTask(tvDao).execute(tableModel);
    }

    private static class insertAsyncTask extends AsyncTask<TvModel, Void, Void> {

        private TvDao mAsyncTaskDao;

        insertAsyncTask(TvDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final TvModel... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private class deleteAsyncTask extends AsyncTask<TvModel, Void, Void> {
        private TvDao mAsyncTaskDao;

        deleteAsyncTask(TvDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final TvModel... params) {
            mAsyncTaskDao.delete(params[0]);
            return null;
        }
    }

    public MutableLiveData<TvResponseModel> searchTv(String apiKey, String language, String query, final Activity activity) {
        final MutableLiveData<TvResponseModel> modelMutableLiveData = new MutableLiveData<>();
        servicesAPI.searchTv(apiKey, language, query).enqueue(new Callback<TvResponseModel>() {
            @Override
            public void onResponse(Call<TvResponseModel> call,
                                   Response<TvResponseModel> response) {
                if (response.isSuccessful()) {
                    modelMutableLiveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<TvResponseModel> call, Throwable t) {
                Log.wtf("ERROR", t.toString());
                Intent intent = new Intent(activity, ErrorActivity.class);
                activity.finish();
                activity.startActivity(intent);
            }
        });
        return modelMutableLiveData;
    }
}
