package in.setone.moviecatalogue5.room;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomWarnings;
import android.content.Context;
import android.support.annotation.NonNull;
import in.setone.moviecatalogue5.alarm.AlarmReceiver;
import in.setone.moviecatalogue5.model.AppSettingModel;
import in.setone.moviecatalogue5.model.MoviesModel;
import in.setone.moviecatalogue5.model.TvModel;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {MoviesModel.class, TvModel.class, AppSettingModel.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract MoviesDao moviesDao();

    public abstract TvDao tvDao();

    public abstract SettingDao settingDao();

    // marking the instance as volatile to ensure atomic access to the variable
    private static volatile AppDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    private static Context getContext;

    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    @SuppressWarnings(RoomWarnings.CANNOT_CREATE_VERIFICATION_DATABASE)
    public static AppDatabase getDatabase(final Context context) {
        getContext = context;
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, "moviecatalog_db")
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    /**
     * Override the onOpen method to populate the database.
     * For this sample, we clear the database every time it is created or opened.
     *
     * If you want to populate the database only when the database is created for the 1st time,
     * override RoomDatabase.Callback()#onCreate
     */
    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db){
            super.onCreate(db);

            // If you want to keep data through app restarts,
            // comment out the following block
            databaseWriteExecutor.execute(() -> {
                // Populate the database in the background.
                // If you want to start with more words, just add them.
                SettingDao dao = INSTANCE.settingDao();
                dao.deleteAll();

                AlarmReceiver.setDaliyRemainder(getContext,AlarmReceiver.ID_REPEATING);
                AlarmReceiver.setDaliyRemainder(getContext, AlarmReceiver.ID_RELEASE_REPEATING);

                AppSettingModel setting = new AppSettingModel(1,1,1);
                dao.insert(setting);
            });
        }

        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);

        }
    };
}
