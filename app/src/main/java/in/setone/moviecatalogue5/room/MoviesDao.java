package in.setone.moviecatalogue5.room;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.*;
import android.database.Cursor;
import in.setone.moviecatalogue5.model.MoviesModel;
import in.setone.moviecatalogue5.parcelable.Movies;

import java.util.List;

@Dao
public interface MoviesDao {

    @Insert (onConflict = OnConflictStrategy.IGNORE)
    void insert(MoviesModel moviesModel);

    @Query("DELETE FROM movies_fav")
    void deleteAll();

    @Delete
    void delete(MoviesModel moviesModel);

    @Query("SELECT * from movies_fav ORDER BY id ASC")
    LiveData<List<Movies>> getResults();

    //for widget
    @Query("SELECT * from movies_fav ORDER BY id ASC")
    List<Movies> getListMovies();

    //for contentprovider
    @Query("SELECT * from movies_fav ORDER BY id ASC")
    Cursor getCursorMovies();

    @Query("SELECT * from movies_fav WHERE id = :id")
    Cursor loadCursorMovie(long id);
}
