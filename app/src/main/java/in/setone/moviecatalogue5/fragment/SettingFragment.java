package in.setone.moviecatalogue5.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.SearchView;
import android.view.*;
import android.widget.CompoundButton;
import android.widget.Switch;
import in.setone.moviecatalogue5.R;
import in.setone.moviecatalogue5.alarm.AlarmReceiver;
import in.setone.moviecatalogue5.model.AppSettingModel;
import in.setone.moviecatalogue5.viewmodel.SettingViewModel;


public class SettingFragment extends Fragment {

    private CardView cVChangeLanguage;
    private Switch dailyRemainder,dailyReleaseRemainder;
    private SettingViewModel settingViewModel;
    private AppSettingModel appSettingModel;
    public SettingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_setting, container, false);

        cVChangeLanguage = rootView.findViewById(R.id.changeLanguage);
        dailyRemainder = rootView.findViewById(R.id.switchDailyRemainder);
        dailyReleaseRemainder = rootView.findViewById(R.id.switchDailyReleaseRemainder);

        setRemainder();

        cVChangeLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Settings.ACTION_LOCALE_SETTINGS);
                startActivity(i);
            }
        });

        dailyRemainder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked){
                    AlarmReceiver.setDaliyRemainder(getContext(),AlarmReceiver.ID_REPEATING);
                    appSettingModel.setDaily_remainder(1);
                    settingViewModel.update(appSettingModel,getActivity());
                }else {
                    AlarmReceiver.cancleRemainder(getContext(),AlarmReceiver.ID_REPEATING);
                    appSettingModel.setDaily_remainder(0);
                    settingViewModel.update(appSettingModel,getActivity());
                }

            }
        });
        dailyReleaseRemainder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked){
                    AlarmReceiver.setDaliyRemainder(getContext(), AlarmReceiver.ID_RELEASE_REPEATING);
                    appSettingModel.setRelease_remainder(1);
                    settingViewModel.update(appSettingModel,getActivity());
                }else {
                    AlarmReceiver.cancleRemainder(getContext(),AlarmReceiver.ID_RELEASE_REPEATING);
                    appSettingModel.setRelease_remainder(0);
                    settingViewModel.update(appSettingModel,getActivity());
                }

            }
        });

        return rootView;
    }

    private void setRemainder() {
        settingViewModel = ViewModelProviders.of(requireActivity()).get(SettingViewModel.class);
        settingViewModel.init(getActivity());
        settingViewModel.getAppSetting().observe(requireActivity(), new Observer<AppSettingModel>() {
            @Override
            public void onChanged(@Nullable AppSettingModel settingModel) {
                appSettingModel = settingModel;
                setupChecker();
            }
        });

    }

    private void setupChecker() {
        if ( appSettingModel.getDaily_remainder() == 1){
            dailyRemainder.setChecked(true);
        }else {
            dailyRemainder.setChecked(false);
        }
        if ( appSettingModel.getRelease_remainder() == 1){
            dailyReleaseRemainder.setChecked(true);
        }else {
            dailyReleaseRemainder.setChecked(false);
        }
    }

    @Override
    public void onCreateOptionsMenu(android.view.Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        item.setVisible(false);
        searchView.setVisibility(View.GONE);
    }


}
