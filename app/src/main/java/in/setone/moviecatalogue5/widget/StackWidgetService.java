package in.setone.moviecatalogue5.widget;

import android.app.Application;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.FutureTarget;
import in.setone.moviecatalogue5.R;
import in.setone.moviecatalogue5.parcelable.Movies;
import in.setone.moviecatalogue5.room.AppDatabase;
import in.setone.moviecatalogue5.room.MoviesDao;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class StackWidgetService extends RemoteViewsService {

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new StackRemoteViewsFactory(this.getApplicationContext(), intent);
    }

    public static void updateWidget(Context context, Application application) {
        Intent intent = new Intent(context, StackWidgetProvider.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        int[] ids = AppWidgetManager.getInstance(application).getAppWidgetIds(new ComponentName(application, StackWidgetProvider.class));
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
        context.sendBroadcast(intent);
    }
}

class StackRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {
    private List<Movies> list = new ArrayList<Movies>();
    private Context mContext;
    private int mAppWidgetId;

    public StackRemoteViewsFactory(Context context, Intent intent) {
        mContext = context;
        mAppWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
    }

    public void onCreate() {
    }

    public void onDestroy() {
        list.clear();
    }

    public int getCount() {
        return list.size();
    }

    public RemoteViews getViewAt(int position) {
        RemoteViews rv = new RemoteViews(mContext.getPackageName(), R.layout.stackwidget_item);
        if (position <= getCount()) {
            Movies movie = list.get(position);

            if (movie.getPoster_path() != null) {
                final String url_image = "https://image.tmdb.org/t/p/w185" + movie.getPoster_path();
                FutureTarget<Bitmap> futureBitmap = Glide.with(mContext)
                        .asBitmap()
                        .load(url_image)
                        .submit();
                try {
                    Bitmap myBitmap = futureBitmap.get();
                    rv.setImageViewBitmap(R.id.stackWidgetItemPoster, myBitmap);
                } catch (ExecutionException | InterruptedException e) {
                    e.printStackTrace();
                }

            }

            if (!movie.getTitle().isEmpty()) {
                rv.setTextViewText(R.id.stackWidgetItemTitle, movie.getTitle());
            }
            rv.setTextViewText(R.id.stackWidgetItemOverview, movie.getOverview());

            Bundle extras = new Bundle();
            Intent fillInIntent = new Intent();
            fillInIntent.putExtras(extras);
            rv.setOnClickFillInIntent(R.id.stackWidgetItem, fillInIntent);
        }

        return rv;
    }
    public RemoteViews getLoadingView() {
        return null;
    }

    public int getViewTypeCount() {
        return 1;
    }

    public long getItemId(int position) {
        return position;
    }

    public boolean hasStableIds() {
        return true;
    }

    public void onDataSetChanged() {
        list.clear();
        MoviesDao moviesDao;
        AppDatabase appDatabase = AppDatabase.getDatabase(mContext);
        appDatabase.getOpenHelper().getWritableDatabase();
        moviesDao = appDatabase.moviesDao();
        list.addAll(moviesDao.getListMovies());
    }
}
