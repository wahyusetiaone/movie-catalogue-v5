package in.setone.moviecatalogue5.populate;

import android.os.AsyncTask;
import in.setone.moviecatalogue5.room.MoviesDao;
import in.setone.moviecatalogue5.room.MoviesRoom;

public class MoviesPopulate extends AsyncTask<Void, Void, Void> {

    private final MoviesDao moviesDao;
    int[] movies = {100, 200, 300};


    public MoviesPopulate(MoviesRoom moviesRoom) {
        moviesDao = moviesRoom.moviesDao();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        // Start the app with a clean database every time.
        // Not needed if you only populate the database
        // when it is first created
        moviesDao.deleteAll();
//
//        for (int i = 0; i <= movies.length - 1; i++) {
//            MoviesModel movie = new MoviesModel(movies[1]);
//            moviesDao.insert(movie);
//            Log.wtf("HAS BEEN INSERT","SUCCESSFULLY");
//        }
        return null;
    }
}
