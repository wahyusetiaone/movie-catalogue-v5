package in.setone.moviecatalogue5.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import in.setone.moviecatalogue5.R;
import in.setone.moviecatalogue5.model.MoviesModel;
import in.setone.moviecatalogue5.parcelable.Movies;

import java.util.ArrayList;
import java.util.List;

public class NotifMovies {
    private int idNotification = 0;
    private List<Movies> stackNotif = new ArrayList<>();
    private static final CharSequence CHANNEL_NAME = "movie channel";
    private final static String GROUP_KEY_EMAILS = "group_key_movies";
    private static final int MAX_NOTIFICATION = 2;
    private int idreleasechannel;


    public void callNotif(List<Movies> list, Context context, int id){
        this.idreleasechannel = id;
        this.idNotification = list.size();
        stackNotif = list;
        sendNotif(context);
    }

    private void sendNotif(Context context) {
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_settings_black_24dp);
//        Intent intent = new Intent(context, MainActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(context, idreleasechannel, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder mBuilder;

        //Melakukan pengecekan jika idNotification lebih kecil dari Max Notif
        String CHANNEL_ID = "channel_01";
        if (idNotification != 0){
            if (idNotification < MAX_NOTIFICATION) {
                mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                        .setContentTitle("Movie release today")
                        .setContentText(stackNotif.get(idNotification).getTitle()+"has been release today")
                        .setSmallIcon(R.drawable.ic_settings_black_24dp)
                        .setLargeIcon(largeIcon)
                        .setGroup(GROUP_KEY_EMAILS)
//                    .setContentIntent(pendingIntent)
                        .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                        .setSound(alarmSound)
                        .setAutoCancel(true);
            } else {
                NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle()
                        .setBigContentTitle(idNotification + " new release")
                        .setSummaryText("movie catalogs");
                for (MoviesModel notif: stackNotif) {
                    inboxStyle.addLine(notif.getTitle()+"has been release today");
                }
                mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                        .setContentTitle(idNotification + " new release")
                        .setContentText("movie catalogs")
                        .setSmallIcon(R.drawable.ic_settings_black_24dp)
                        .setGroup(GROUP_KEY_EMAILS)
                        .setGroupSummary(true)
//                    .setContentIntent(pendingIntent)
                        .setStyle(inboxStyle)
                        .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                        .setSound(alarmSound)
                        .setAutoCancel(true);
            }
         /*
        Untuk android Oreo ke atas perlu menambahkan notification channel
        Materi ini akan dibahas lebih lanjut di modul extended
         */
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                /* Create or update. */
                NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                        CHANNEL_NAME,
                        NotificationManager.IMPORTANCE_DEFAULT);

                mBuilder.setChannelId(CHANNEL_ID);

                if (mNotificationManager != null) {
                    mNotificationManager.createNotificationChannel(channel);
                }
            }

            Notification notification = mBuilder.build();

            if (mNotificationManager != null) {
                mNotificationManager.notify(idNotification, notification);
            }
        }
    }
}
