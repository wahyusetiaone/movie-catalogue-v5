package in.setone.moviecatalogue5.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.*;
import android.widget.ProgressBar;
import in.setone.moviecatalogue5.R;
import in.setone.moviecatalogue5.activity.MainActivity;
import in.setone.moviecatalogue5.adapter.MoviesListAdapter;
import in.setone.moviecatalogue5.model.MoviesResponseModel;
import in.setone.moviecatalogue5.parcelable.Movies;
import in.setone.moviecatalogue5.viewmodel.MoviesViewModel;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class MovieFragment extends Fragment {

    private RecyclerView rcMovies;
    private List<Movies> listMovies = new ArrayList<>();
    private List<Movies> listMoviesDb = new ArrayList<>();
    private MoviesListAdapter moviesListAdapter;
    private ProgressBar progressBar;

    private MoviesViewModel moviesViewModel;

    private static int spin;

    public MovieFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_movie, container, false);
        super.onViewCreated(rootView, savedInstanceState);
        progressBar = rootView.findViewById(R.id.progressBarMovies);
        rcMovies = rootView.findViewById(R.id.rvMovie);

        showLoading(true);

        rcMovies.setHasFixedSize(true);
        rcMovies.setLayoutManager(new LinearLayoutManager(getContext()));

        moviesViewModel = ViewModelProviders.of(requireActivity()).get(MoviesViewModel.class);

        requestDataMovies(false);

        return rootView;

        //TODO:Saat search lihat fav sebagian fav tidak ada bintang karena if pada adapter rv
        //TODO:Saat search lihat fav dia fokus ke fav, gunakan if search ada value nya clearFocus();
    }

    @Override
    public void onCreateOptionsMenu(android.view.Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        if (searchView != null){
            searchView.clearFocus();
            searchView.onActionViewCollapsed();
        }
        item.setVisible(true);
        searchView.setQueryHint(getResources().getString(R.string.searchMov));
        searchView.setVisibility(View.VISIBLE);
        searchListener(searchView);
    }

    private void searchListener(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                moviesViewModel.searchMoviesApi(s,getActivity()).observe(requireActivity(), new Observer<MoviesResponseModel>() {
                    @Override
                    public void onChanged(@Nullable MoviesResponseModel moviesResponseModel) {
                        listMovies.clear();
                        List<Movies> results = moviesResponseModel.getResults();
                        listMovies.addAll(results);
                        showRecyclerList();
                    }
                });
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                requestDataMovies(true);
                return false;
            }
        });

    }

    private void requestDataMovies(Boolean reload) {
        moviesViewModel.init(getActivity(), reload);
        moviesViewModel.getMoviesApi().observe(requireActivity(), new Observer<MoviesResponseModel>() {
            @Override
            public void onChanged(@NotNull MoviesResponseModel moviesResponse) {
                listMovies.clear();
                List<Movies> results = moviesResponse.getResults();
                listMovies.addAll(results);
                showRecyclerList();
            }
        });
        moviesViewModel.getMoviesDatabase().observe(requireActivity(), new Observer<List<Movies>>() {
            @Override
            public void onChanged(@Nullable List<Movies> moviesModels) {
                listMoviesDb.clear();
                listMoviesDb.addAll(moviesModels);
                showRecyclerList();
            }
        });
    }

    private void showRecyclerList() {
        int spinner = MainActivity.spin;

        if (spinner == 1 && spinner != spin || moviesListAdapter == null) {
            moviesListAdapter = new MoviesListAdapter(listMovies, getActivity(), listMoviesDb);
            moviesListAdapter.addContext(getContext(), false);
            rcMovies.setAdapter(moviesListAdapter);
            spin = spinner;
        } else if (spinner == 2 && spinner != spin) {
            moviesListAdapter = new MoviesListAdapter(listMoviesDb, getActivity(), listMovies);
            moviesListAdapter.addContext(getContext(),true);
            rcMovies.setAdapter(moviesListAdapter);
            spin = spinner;
        }else if(moviesListAdapter != null) {
            moviesListAdapter.notifyDataSetChanged();
        }
        showLoading(false);
    }

    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
            rcMovies.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            rcMovies.setVisibility(View.VISIBLE);
        }
    }

}
