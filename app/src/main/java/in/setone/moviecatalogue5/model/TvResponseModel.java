package in.setone.moviecatalogue5.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import in.setone.moviecatalogue5.parcelable.TvSeries;

import java.util.List;

public class TvResponseModel {
    @SerializedName("page")
    @Expose
    private int page;
    @SerializedName("total_results")
    @Expose
    private int total_results;
    @SerializedName("total_pages")
    @Expose
    private int total_pages;
    @SerializedName("results")
    @Expose
    private List<TvSeries> results = null;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotal_results() {
        return total_results;
    }

    public void setTotal_results(int total_results) {
        this.total_results = total_results;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    public List<TvSeries> getResults() {
        return results;
    }

    public void setResults(List<TvSeries> results) {
        this.results = results;
    }
}
