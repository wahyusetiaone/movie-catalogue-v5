package in.setone.moviecatalogue5.alarm;

import android.app.*;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;
import in.setone.moviecatalogue5.BuildConfig;
import in.setone.moviecatalogue5.R;
import in.setone.moviecatalogue5.model.MoviesResponseModel;
import in.setone.moviecatalogue5.network.RetrofitService;
import in.setone.moviecatalogue5.network.ServicesAPI;
import in.setone.moviecatalogue5.notification.NotifMovies;
import in.setone.moviecatalogue5.parcelable.Movies;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
//TODO::Retofit onResponse not called
public class AlarmReceiver extends BroadcastReceiver {

    public static final String TYPE_REPEATING_RELEASE = "Release Daily Remainder";
    public static final String TYPE_REPEATING = "Daily Remainder";
    public static final String EXTRA_MESSAGE = "message";
    public static final String EXTRA_TYPE = "type";
    private static final String TIME_REPEATING_RELEASE = "07:30";
    private static final String TIME_REPEATING = "07:00";

    private final String API_KEY = BuildConfig.TMDB_API_KEY;
    private final String EN_LANGUANGE = "en-US";
    private final String ID_LANGUANGE = "id-ID";

    public static final int ID_RELEASE_REPEATING = 100;
    public static final int ID_REPEATING = 101;

    private String TIME_FORMAT = "HH:mm";

    public AlarmReceiver() {
    }

    public static void setDaliyRemainder(Context context, int id){
        AlarmReceiver alarmReceiver = new AlarmReceiver();

        String msg = id == (ID_REPEATING) ? TYPE_REPEATING : TYPE_REPEATING_RELEASE;
        String type = id == (ID_REPEATING) ? TYPE_REPEATING : TYPE_REPEATING_RELEASE;
        String time = id == (ID_REPEATING) ? TIME_REPEATING : TIME_REPEATING_RELEASE;

        alarmReceiver.setRepeatingAlarm(context, type ,time, msg);
    }

    public static void cancleRemainder(Context context,int id){

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, id, intent, 0);
        if (alarmManager != null) {
            alarmManager.cancel(pendingIntent);
        }
        String msg = id == (ID_REPEATING) ? TYPE_REPEATING : TYPE_REPEATING_RELEASE;
        Toast.makeText(context, msg + " has been Cancel", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String type = intent.getStringExtra(EXTRA_TYPE);
        String message = intent.getStringExtra(EXTRA_MESSAGE);

        String title = type.equalsIgnoreCase(TYPE_REPEATING_RELEASE) ? TYPE_REPEATING_RELEASE : TYPE_REPEATING;
        int notifId = type.equalsIgnoreCase(TYPE_REPEATING_RELEASE) ? ID_RELEASE_REPEATING : ID_REPEATING;

        showToast(context, title, message);

        if (type.equals(TYPE_REPEATING)) {
            showAlarmNotification(context, title, message, notifId);
        }else {
            NotifMovies notifMovies = new  NotifMovies();
            final List<Movies> list = new ArrayList<>();
            String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

            ServicesAPI servicesAPI = RetrofitService.createService(ServicesAPI.class);
            servicesAPI.getReleaseMovies(API_KEY,date,date).enqueue(new Callback<MoviesResponseModel>() {
                @Override
                public void onResponse(Call<MoviesResponseModel> call, Response<MoviesResponseModel> response) {
                    Log.wtf("Response", String.valueOf(response.isSuccessful()));
                    Log.wtf("MSG",response.message());
                    list.clear();
                    if (response.isSuccessful()) {
                        list.addAll(response.body().getResults());
                        notifMovies.callNotif(list,context,ID_RELEASE_REPEATING);
                    }
                }

                @Override
                public void onFailure(Call<MoviesResponseModel> call, Throwable t) {
                    Log.e("ERROR",t.toString());
                }
            });
//            try {
//                list.clear();
//                Response<MoviesResponseModel> response = servicesAPI.getReleaseMovies(API_KEY,date,date).execute();
//                list.addAll(response.body().getResults());
//            } catch (IOException e) {
//                e.printStackTrace();
//            }

//            for (int i = 0; i < 5; i++){
//                list.add(new NotifMovieModel(i,"name "+String.valueOf(i)));
//            }
        }
    }

    private void showToast(Context context, String title, String message) {
        Toast.makeText(context, title + " : " + message, Toast.LENGTH_LONG).show();
    }

    private void showAlarmNotification(Context context, String title, String message, int notifId) {
        String CHANNEL_ID = "Channel_1";
        String CHANNEL_NAME = "AlarmManager channel";

        NotificationManager notificationManagerCompat = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_settings_black_24dp)
                .setContentTitle(title)
                .setContentText(message)
                .setColor(ContextCompat.getColor(context, android.R.color.transparent))
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setSound(alarmSound);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_DEFAULT);

            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{1000, 1000, 1000, 1000, 1000});

            builder.setChannelId(CHANNEL_ID);

            if (notificationManagerCompat != null) {
                notificationManagerCompat.createNotificationChannel(channel);
            }
        }

        Notification notification = builder.build();

        if (notificationManagerCompat != null) {
            notificationManagerCompat.notify(notifId, notification);
        }

    }

    public void setRepeatingAlarm(Context context, String type, String time, String message) {

        if (isDateInvalid(time, TIME_FORMAT)) return;

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra(EXTRA_MESSAGE, message);
        intent.putExtra(EXTRA_TYPE, type);

        String timeArray[] = time.split(":");

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeArray[0]));
        calendar.set(Calendar.MINUTE, Integer.parseInt(timeArray[1]));
        calendar.set(Calendar.SECOND, 0);

        int notifId = type.equalsIgnoreCase(TYPE_REPEATING_RELEASE) ? ID_RELEASE_REPEATING : ID_REPEATING;

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, notifId, intent, 0);
        if (alarmManager != null) {
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        }

//        showToast(context,"Remainder Setup",type);

    }

    public boolean isDateInvalid(String date, String format) {
        try {
            DateFormat df = new SimpleDateFormat(format, Locale.getDefault());
            df.setLenient(false);
            df.parse(date);
            return false;
        } catch (ParseException e) {
            return true;
        }
    }
}
