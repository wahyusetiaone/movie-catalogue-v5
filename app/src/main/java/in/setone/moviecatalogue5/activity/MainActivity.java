package in.setone.moviecatalogue5.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.support.v7.widget.SearchView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import in.setone.moviecatalogue5.R;
import in.setone.moviecatalogue5.fragment.MovieFragment;
import in.setone.moviecatalogue5.fragment.SettingFragment;
import in.setone.moviecatalogue5.fragment.TvSeriesFragment;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private FragmentTransaction ft;
    private final String MY_FRAGMENT_TAG = "CURRENT_FRAGMENT";
    private final String KEY_LOCALE = "LOCALE";
    private SharedPreferences sharedPref;
    public static int spin;

    private BottomNavigationView navigation;
    private Spinner spinner;

    //TODO:Learn Navigation Component

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_movies:
                    showDisabelMenu(R.id.navigation_movies);
                    ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frameLayout, new MovieFragment(), MY_FRAGMENT_TAG);
                    ft.commit();
                    return true;
                case R.id.navigation_tvseries:
                    showDisabelMenu(R.id.navigation_tvseries);
                    ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frameLayout, new TvSeriesFragment(), MY_FRAGMENT_TAG);
                    ft.commit();
                    return true;
                case R.id.navigation_setting:
                    showDisabelMenu(R.id.navigation_setting);
                    ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frameLayout, new SettingFragment(), MY_FRAGMENT_TAG);
                    ft.commit();
                    return true;
            }
            return false;
        }
    };

    private void showDisabelMenu(int navigation_movies) {

        resetSpinner();

        switch (navigation_movies) {
            case R.id.navigation_movies:
                navigation.getMenu().findItem(R.id.navigation_movies).setEnabled(false);
                navigation.getMenu().findItem(R.id.navigation_tvseries).setEnabled(true);
                navigation.getMenu().findItem(R.id.navigation_setting).setEnabled(true);
                spinner.setVisibility(View.VISIBLE);
                break;
            case R.id.navigation_tvseries:
                navigation.getMenu().findItem(R.id.navigation_movies).setEnabled(true);
                navigation.getMenu().findItem(R.id.navigation_tvseries).setEnabled(false);
                navigation.getMenu().findItem(R.id.navigation_setting).setEnabled(true);
                spinner.setVisibility(View.VISIBLE);
                break;
            case R.id.navigation_setting:
                navigation.getMenu().findItem(R.id.navigation_movies).setEnabled(true);
                navigation.getMenu().findItem(R.id.navigation_tvseries).setEnabled(true);
                navigation.getMenu().findItem(R.id.navigation_setting).setEnabled(false);
                spinner.setVisibility(View.GONE);
                break;
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinnerSetup();

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if (savedInstanceState == null) {
            navigation.setSelectedItemId(R.id.navigation_tvseries);
        }

        //for saving and catching when locale has been changed
        sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        readLocale();

    }


    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setIconified(false);
//        if (searchView != null){
//            searchView.setQuery("", false);
//            searchView.clearFocus();
//            searchView.onActionViewCollapsed();
//        }
        return true;
    }

    private void spinnerSetup() {
        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.menu, R.layout.item_spinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        resetSpinner();
    }

    private void resetSpinner() {
        spinner.setSelection(0);
        spin = 0;
    }

    private void readLocale() {
        String locate = sharedPref.getString(KEY_LOCALE, "missing");

        if (locate.equals("missing")) {
            localeChange();
        } else if (!locate.equals(Locale.getDefault().getDisplayLanguage())) {
            localeChange();
            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }
    }

    private void localeChange() {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(KEY_LOCALE, Locale.getDefault().getDisplayLanguage());
        editor.apply();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // Reload current fragment
        Fragment frg = null;
        frg = getSupportFragmentManager().findFragmentByTag(MY_FRAGMENT_TAG);
        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.detach(frg);
        ft.attach(frg);
        ft.commit();

        spin = position + 1;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}
