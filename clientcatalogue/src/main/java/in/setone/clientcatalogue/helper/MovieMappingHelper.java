package in.setone.clientcatalogue.helper;

import android.database.Cursor;
import android.net.Uri;
import in.setone.clientcatalogue.parceable.MovieParcel;

import java.util.ArrayList;

public class MovieMappingHelper {

    public static Uri URI_CONTENT_MOVIE = Uri.parse("content://in.setone.moviecatalogue5/movies");

    //column db name
    protected static String DB_ID = "id";
    protected static String DB_POPULARITY = "popularity";
    protected static String DB_VOTE_COUNT = "vote_count";
    protected static String DB_VIDEO = "video";
    protected static String DB_POSTER = "poster_path";
    protected static String DB_ADULT = "adult";
    protected static String DB_BACKDROP_PATH = "backdrop_path";
    protected static String DB_ORIGINAL_LANGUAGE = "original_language";
    protected static String DB_ORIGINAL_TITLE = "original_title";
    protected static String DB_TITLE = "title";
    protected static String DB_VOTE_AVARAGE = "vote_average";
    protected static String DB_OVERVIEW = "overview";
    protected static String DB_RELEASE_DATE = "release_date";
    
    public static ArrayList<MovieParcel> mapCursorToArrayList(Cursor moviesCursor) {
        ArrayList<MovieParcel> notesList = new ArrayList<>();

        while (moviesCursor.moveToNext()) {
            int id = moviesCursor.getInt(moviesCursor.getColumnIndexOrThrow(DB_ID));
            double popularity = moviesCursor.getDouble(moviesCursor.getColumnIndexOrThrow(DB_POPULARITY));
            int vote_count = moviesCursor.getInt(moviesCursor.getColumnIndexOrThrow(DB_VOTE_COUNT));
            boolean video = moviesCursor.getInt(moviesCursor.getColumnIndexOrThrow(DB_VIDEO)) > 0;
            String poster_path= moviesCursor.getString(moviesCursor.getColumnIndexOrThrow(DB_POSTER));
            boolean adult = moviesCursor.getInt(moviesCursor.getColumnIndexOrThrow(DB_ADULT)) > 0;
            String backdrop_path = moviesCursor.getString(moviesCursor.getColumnIndexOrThrow(DB_BACKDROP_PATH));
            String original_language = moviesCursor.getString(moviesCursor.getColumnIndexOrThrow(DB_ORIGINAL_LANGUAGE));
            String original_title = moviesCursor.getString(moviesCursor.getColumnIndexOrThrow(DB_ORIGINAL_TITLE));
            String title = moviesCursor.getString(moviesCursor.getColumnIndexOrThrow(DB_TITLE));
            double vote_average = moviesCursor.getDouble(moviesCursor.getColumnIndexOrThrow(DB_VOTE_AVARAGE));
            String overview = moviesCursor.getString(moviesCursor.getColumnIndexOrThrow(DB_OVERVIEW));
            String release_date = moviesCursor.getString(moviesCursor.getColumnIndexOrThrow(DB_RELEASE_DATE));
            notesList.add(new MovieParcel(id,popularity,vote_count,video,poster_path,adult,backdrop_path,original_language,original_title,title,vote_average,overview,release_date));
        }

        return notesList;
    }

    public static MovieParcel mapCursorToObject(Cursor moviesCursor) {
        moviesCursor.moveToFirst();
        int id = moviesCursor.getInt(moviesCursor.getColumnIndexOrThrow(DB_ID));
        double popularity = moviesCursor.getDouble(moviesCursor.getColumnIndexOrThrow(DB_POPULARITY));
        int vote_count = moviesCursor.getInt(moviesCursor.getColumnIndexOrThrow(DB_VOTE_COUNT));
        boolean video = moviesCursor.getInt(moviesCursor.getColumnIndexOrThrow(DB_VIDEO)) > 0;
        String poster_path= moviesCursor.getString(moviesCursor.getColumnIndexOrThrow(DB_POSTER));
        boolean adult = moviesCursor.getInt(moviesCursor.getColumnIndexOrThrow(DB_ADULT)) > 0;
        String backdrop_path = moviesCursor.getString(moviesCursor.getColumnIndexOrThrow(DB_BACKDROP_PATH));
        String original_language = moviesCursor.getString(moviesCursor.getColumnIndexOrThrow(DB_ORIGINAL_LANGUAGE));
        String original_title = moviesCursor.getString(moviesCursor.getColumnIndexOrThrow(DB_ORIGINAL_TITLE));
        String title = moviesCursor.getString(moviesCursor.getColumnIndexOrThrow(DB_TITLE));
        double vote_average = moviesCursor.getDouble(moviesCursor.getColumnIndexOrThrow(DB_VOTE_AVARAGE));
        String overview = moviesCursor.getString(moviesCursor.getColumnIndexOrThrow(DB_OVERVIEW));
        String release_date = moviesCursor.getString(moviesCursor.getColumnIndexOrThrow(DB_RELEASE_DATE));

        return new MovieParcel(id,popularity,vote_count,video,poster_path,adult,backdrop_path,original_language,original_title,title,vote_average,overview,release_date);
    }
}
