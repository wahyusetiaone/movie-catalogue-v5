package in.setone.clientcatalogue.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import in.setone.clientcatalogue.R;
import in.setone.clientcatalogue.R.string;
import in.setone.clientcatalogue.adapter.TvSeriesListAdapter;
import in.setone.clientcatalogue.parceable.TvParcel;

public class DetailsTvSeriesActivity extends AppCompatActivity {

    private TvParcel data;
    private static boolean fav;

    private TextView tvTitle,tvDetails,tvLabel;
    private ImageView ivTvSeries,ivFav;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_tv_series);

        customActionBar();

        initializeComponent();

        showLoading(true);

        Intent intent = getIntent();
        data = intent.getParcelableExtra(TvSeriesListAdapter.KEY_OF_TVSERIES);

        setData();

    }

    private void customActionBar() {
        this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar);
        //getSupportActionBar().setElevation(0);
        View view = getSupportActionBar().getCustomView();
        TextView name = view.findViewById(R.id.tvActionBar);
        name.setText(R.string.details_tv);
        ImageView imageView = view.findViewById(R.id.ivBackActionBar);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void setData() {

        tvTitle.setText(data.getName());
        if (data.getOverview().equals("")){
            tvDetails.setText(R.string.no_details);
        }else {
            tvDetails.setText(data.getOverview());
        }

        final String url_image = "https://image.tmdb.org/t/p/w185" + data.getPoster_path();
        Picasso.get().load(url_image).into(ivTvSeries);

        //dummy load
        new CountDownTimer(3000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                showLoading(false);
            }
        }.start();

    }

    private void initializeComponent() {

        tvDetails = (TextView) findViewById(R.id.tvTvSeriesDetalis);
        tvLabel = (TextView) findViewById(R.id.tvTDetais);
        tvTitle = (TextView) findViewById(R.id.tvTvSeriesTitle);
        progressBar = (ProgressBar) findViewById(R.id.progressBarTDetails);
        ivTvSeries = (ImageView) findViewById(R.id.ivImgTvSeries);

    }

    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
            tvDetails.setVisibility(View.GONE);
            tvLabel.setVisibility(View.GONE);
            tvTitle.setVisibility(View.GONE);
            ivTvSeries.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            tvDetails.setVisibility(View.VISIBLE);
            tvLabel.setVisibility(View.VISIBLE);
            tvTitle.setVisibility(View.VISIBLE);
            ivTvSeries.setVisibility(View.VISIBLE);
        }
    }
}
