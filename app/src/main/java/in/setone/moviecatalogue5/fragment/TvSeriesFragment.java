package in.setone.moviecatalogue5.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.*;
import android.widget.ProgressBar;
import in.setone.moviecatalogue5.R;
import in.setone.moviecatalogue5.activity.MainActivity;
import in.setone.moviecatalogue5.adapter.TvSeriesListAdapter;
import in.setone.moviecatalogue5.model.TvResponseModel;
import in.setone.moviecatalogue5.parcelable.TvSeries;
import in.setone.moviecatalogue5.viewmodel.TvViewModel;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;


public class TvSeriesFragment extends Fragment {

    private RecyclerView rcTvSeries;
    private List<TvSeries> listTv = new ArrayList<>();
    private List<TvSeries> listTvDb = new ArrayList<>();
    private ProgressBar progressBar;

    private TvViewModel tvViewModel;
    private TvSeriesListAdapter tvListAdapter;

    private static int spin;

    public TvSeriesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_tv_series, container, false);

        progressBar = rootView.findViewById(R.id.progressBarTv);
        rcTvSeries = rootView.findViewById(R.id.rvTvSeries);

        showLoading(true);

        rcTvSeries.setLayoutManager(new LinearLayoutManager(getContext()));
        rcTvSeries.setHasFixedSize(true);

        tvViewModel = ViewModelProviders.of(requireActivity()).get(TvViewModel.class);

        requestDataMovies(false);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(android.view.Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        if (searchView != null){
            searchView.clearFocus();
            searchView.onActionViewCollapsed();
        }
        item.setVisible(true);
        searchView.setQueryHint(getResources().getString(R.string.searchTv));
        searchView.setVisibility(View.VISIBLE);
        searchListener(searchView);
    }

    private void searchListener(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                tvViewModel.searchTvApi(s,getActivity()).observe(requireActivity(), new Observer<TvResponseModel>() {
                    @Override
                    public void onChanged(@Nullable TvResponseModel tvResponseModel) {
                        listTv.clear();
                        List<TvSeries> results = tvResponseModel.getResults();
                        listTv.addAll(results);
                        showRecyclerList();
                    }
                });
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                requestDataMovies(true);
                return false;
            }
        });

    }

    private void requestDataMovies(boolean reload) {

        tvViewModel.init(getActivity(), reload);
        tvViewModel.getTvApi().observe(requireActivity(), new Observer<TvResponseModel>() {
            @Override
            public void onChanged(@NotNull TvResponseModel tvResponse) {
                listTv.clear();
                List<TvSeries> results = tvResponse.getResults();
                listTv.addAll(results);
                showRecyclerList();
            }
        });
        tvViewModel.getTvDatabase().observe(requireActivity(), new Observer<List<TvSeries>>() {
            @Override
            public void onChanged(@Nullable List<TvSeries> tvSeries) {
                listTvDb.clear();
                listTvDb.addAll(tvSeries);
                showRecyclerList();
            }
        });

    }

    private void showRecyclerList() {
        int spinner = MainActivity.spin;

        if (spinner == 1 && spinner != spin || tvListAdapter == null) {
            tvListAdapter = new TvSeriesListAdapter(listTv, getActivity(), listTvDb);
            tvListAdapter.addContext(getContext(), false);
            rcTvSeries.setAdapter(tvListAdapter);
            spin = spinner;
        } else if (spinner == 2 && spinner != spin) {
            tvListAdapter = new TvSeriesListAdapter(listTvDb, getActivity(), listTv);
            tvListAdapter.addContext(getContext(), true);
            rcTvSeries.setAdapter(tvListAdapter);
            spin = spinner;
        }else if(tvListAdapter != null) {
            tvListAdapter.notifyDataSetChanged();
        }
        showLoading(false);
    }

    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
            rcTvSeries.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            rcTvSeries.setVisibility(View.VISIBLE);
        }
    }
}
