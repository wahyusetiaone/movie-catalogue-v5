package in.setone.clientcatalogue.helper;

import android.database.Cursor;
import android.net.Uri;
import in.setone.clientcatalogue.parceable.TvParcel;

import java.util.ArrayList;

public class TvMappingHelper {

    public static Uri URI_CONTENT_TV = Uri.parse("content://in.setone.moviecatalogue5/tv");

    //column db name
    protected static String DB_ID = "id";
    protected static String DB_ORIGINAL_NAME = "original_name";
    protected static String DB_NAME = "name";
    protected static String DB_POPULARITY = "popularity";
    protected static String DB_VOTE_COUNT = "vote_count";
    protected static String DB_FIRST_AIR_DATE = "first_air_date";
    protected static String DB_BACKDROP_PATH = "backdrop_path";
    protected static String DB_ORIGINAL_LANGUAGE = "original_language";
    protected static String DB_VOTE_AVERAGE = "vote_average";
    protected static String DB_OVERVIEW = "overview";
    protected static String DB_POSTER_PATH = "poster_path";

    protected int id;
    protected String original_name;
    protected String name;
    protected double popularity;
    protected int vote_count;
    protected String first_air_date;
    protected String backdrop_path;
    protected String original_language;
    protected double vote_average;
    protected String overview;
    protected String poster_path;

    public static ArrayList<TvParcel> mapCursorToArrayList(Cursor tvCursor) {
        ArrayList<TvParcel> notesList = new ArrayList<>();

        while (tvCursor.moveToNext()) {
            int id = tvCursor.getInt(tvCursor.getColumnIndexOrThrow(DB_ID));
            String original_name = tvCursor.getString(tvCursor.getColumnIndexOrThrow(DB_ORIGINAL_NAME));
            String name = tvCursor.getString(tvCursor.getColumnIndexOrThrow(DB_NAME));
            double popularity = tvCursor.getDouble(tvCursor.getColumnIndexOrThrow(DB_POPULARITY));
            int vote_count = tvCursor.getInt(tvCursor.getColumnIndexOrThrow(DB_VOTE_COUNT));
            String first_air_date = tvCursor.getString(tvCursor.getColumnIndexOrThrow(DB_FIRST_AIR_DATE));
            String backdrop_path = tvCursor.getString(tvCursor.getColumnIndexOrThrow(DB_BACKDROP_PATH));
            String original_language = tvCursor.getString(tvCursor.getColumnIndexOrThrow(DB_ORIGINAL_LANGUAGE));
            double vote_average = tvCursor.getDouble(tvCursor.getColumnIndexOrThrow(DB_VOTE_AVERAGE));
            String overview = tvCursor.getString(tvCursor.getColumnIndexOrThrow(DB_OVERVIEW));
            String poster_path = tvCursor.getString(tvCursor.getColumnIndexOrThrow(DB_POSTER_PATH));
            notesList.add(new TvParcel(id,original_name,name,popularity,vote_count,first_air_date,backdrop_path,original_language,vote_average,overview,poster_path));
        }

        return notesList;
    }

    public static TvParcel mapCursorToObject(Cursor tvCursor) {
        tvCursor.moveToFirst();
        int id = tvCursor.getInt(tvCursor.getColumnIndexOrThrow(DB_ID));
        String original_name = tvCursor.getString(tvCursor.getColumnIndexOrThrow(DB_ORIGINAL_NAME));
        String name = tvCursor.getString(tvCursor.getColumnIndexOrThrow(DB_NAME));
        double popularity = tvCursor.getDouble(tvCursor.getColumnIndexOrThrow(DB_POPULARITY));
        int vote_count = tvCursor.getInt(tvCursor.getColumnIndexOrThrow(DB_VOTE_COUNT));
        String first_air_date = tvCursor.getString(tvCursor.getColumnIndexOrThrow(DB_FIRST_AIR_DATE));
        String backdrop_path = tvCursor.getString(tvCursor.getColumnIndexOrThrow(DB_BACKDROP_PATH));
        String original_language = tvCursor.getString(tvCursor.getColumnIndexOrThrow(DB_ORIGINAL_LANGUAGE));
        double vote_average = tvCursor.getDouble(tvCursor.getColumnIndexOrThrow(DB_VOTE_AVERAGE));
        String overview = tvCursor.getString(tvCursor.getColumnIndexOrThrow(DB_OVERVIEW));
        String poster_path = tvCursor.getString(tvCursor.getColumnIndexOrThrow(DB_POSTER_PATH));

        return new TvParcel(id,original_name,name,popularity,vote_count,first_air_date,backdrop_path,original_language,vote_average,overview,poster_path);
    }
}
