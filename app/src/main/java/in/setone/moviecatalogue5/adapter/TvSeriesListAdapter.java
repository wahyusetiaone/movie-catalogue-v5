package in.setone.moviecatalogue5.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import in.setone.moviecatalogue5.R;
import in.setone.moviecatalogue5.activity.DetailsTvSeriesActivity;
import in.setone.moviecatalogue5.model.TvModel;
import in.setone.moviecatalogue5.parcelable.TvSeries;
import in.setone.moviecatalogue5.viewmodel.TvViewModel;

import java.util.List;

public class TvSeriesListAdapter extends RecyclerView.Adapter<TvSeriesListAdapter.ListViewHolder> {

    private List<TvSeries> arrayTvSeries;
    private List<TvSeries> dbTv;
    private Context context;
    private Activity activity;
    private boolean fav = false;

    public static String KEY_OF_TVSERIES = "DATA_TV";
    public static String KEY_OF_TV_FAVORITE = "FAV_TV";

    public TvSeriesListAdapter(){}

    public TvSeriesListAdapter(List<TvSeries> tvSeriesArrayList, Activity activity, List<TvSeries> db){
        this.arrayTvSeries = tvSeriesArrayList;
        this.activity = activity;
        this.dbTv = db;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row_tvseries, viewGroup, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder listViewHolder, int i) {
        final TvSeries tvSeries = arrayTvSeries.get(i);
        boolean fav;

        String url_image = "https://image.tmdb.org/t/p/w185" + tvSeries.getPoster_path();
        Picasso.get().load(url_image).into(listViewHolder.imgPoster);

        listViewHolder.tvTitle.setText(tvSeries.getName());

        listViewHolder.tvDetail.setText(tvSeries.getOverview());

        if (tvSeries.getOverview().equals("")){
            listViewHolder.tvDetail.setText(R.string.no_detalis);
        }

        fav = setFavButton(listViewHolder.btnFav,dbTv,tvSeries);

        listViewHolder.cVTvSeries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, DetailsTvSeriesActivity.class);
                i.putExtra(KEY_OF_TVSERIES, tvSeries);
                i.putExtra(KEY_OF_TV_FAVORITE,fav);
                context.startActivity(i);
            }
        });

        listViewHolder.btnFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                favImage(v,tvSeries);
            }
        });
    }

    @SuppressLint("Assert")
    private void favImage(View view, TvSeries tvSeries) {

        TvViewModel viewModel = new TvViewModel();

        ImageView imageView = (ImageView) view;
        assert(R.id.btnFavTv == imageView.getId());

        Integer integer = (Integer) imageView.getTag();
        integer = integer == null ? 0 : integer;

        switch(integer) {
            case R.drawable.ic_star_black_24dp:
                viewModel.delete(tvSeries,activity);
                break;
            case R.drawable.ic_star_border_black_24dp:
                viewModel.insert(tvSeries,activity);
                break;
        }
    }

    private boolean setFavButton(ImageView imageView, List<TvSeries> dbTv, TvSeries tvSeries) {

        imageView.setImageResource(R.drawable.ic_star_border_black_24dp);
        imageView.setTag(R.drawable.ic_star_border_black_24dp);

        for (TvModel model: dbTv) {
            if (tvSeries.getId() == model.getId()){
                imageView.setImageResource(R.drawable.ic_star_black_24dp);
                imageView.setTag(R.drawable.ic_star_black_24dp);
                return true;
            }
        }
        if(fav){
            imageView.setImageResource(R.drawable.ic_star_black_24dp);
            imageView.setTag(R.drawable.ic_star_black_24dp);
        }
        return false;
    }

    @Override
    public int getItemCount() {
        return arrayTvSeries.size();
    }

    public void addContext(Context context, boolean b) {
        this.context = context;
        this.fav = b;
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {

        ImageView imgPoster,btnFav;
        TextView tvTitle,tvDetail;
        CardView cVTvSeries;

        public ListViewHolder(@NonNull View itemView) {
            super(itemView);

            btnFav = itemView.findViewById(R.id.btnFavTv);
            imgPoster = itemView.findViewById(R.id.imgPosterTvSeries);
            tvTitle = itemView.findViewById(R.id.tvTitleTvSeries);
            tvDetail = itemView.findViewById(R.id.tvDetailTvSeries);
            cVTvSeries = itemView.findViewById(R.id.cVTV);
        }
    }
}
