package in.setone.moviecatalogue5.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "app_setting")
public class AppSettingModel {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    @SerializedName("id")
    @Expose
    protected int id;

    @ColumnInfo(name = "daily_remainder")
    @SerializedName("daily_remainder")
    @Expose
    protected int daily_remainder;

    @ColumnInfo(name = "release_remainder")
    @SerializedName("release_remainder")
    @Expose
    protected int release_remainder;

    public AppSettingModel(){}

    public AppSettingModel(int id, int daily_remainder, int release_remainder) {
        this.id = id;
        this.daily_remainder = daily_remainder;
        this.release_remainder = release_remainder;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDaily_remainder() {
        return daily_remainder;
    }

    public void setDaily_remainder(int daily_remainder) {
        this.daily_remainder = daily_remainder;
    }

    public int getRelease_remainder() {
        return release_remainder;
    }

    public void setRelease_remainder(int release_remainder) {
        this.release_remainder = release_remainder;
    }
}
