package in.setone.moviecatalogue5.room;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.*;
import in.setone.moviecatalogue5.model.AppSettingModel;

@Dao
public interface SettingDao {

    @Query("DELETE FROM app_setting")
    void deleteAll();

    @Query("SELECT * from app_setting")
    LiveData<AppSettingModel> getSetting();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(AppSettingModel model);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void update(AppSettingModel model);
}
