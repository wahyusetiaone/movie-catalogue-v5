package in.setone.clientcatalogue.fragment;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ProgressBar;
import in.setone.clientcatalogue.R;
import in.setone.clientcatalogue.adapter.TvSeriesListAdapter;
import in.setone.clientcatalogue.helper.TvMappingHelper;
import in.setone.clientcatalogue.parceable.TvParcel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TvSeriesFragment extends Fragment {

    private RecyclerView rcTvSeries;
    private List<TvParcel> listTvDb = new ArrayList<>();
    private ProgressBar progressBar;

    private TvSeriesListAdapter tvListAdapter;


    public TvSeriesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_tv_series, container, false);

        progressBar = rootView.findViewById(R.id.progressBarTv);
        rcTvSeries = rootView.findViewById(R.id.rvTvSeries);

        rcTvSeries.setHasFixedSize(true);
        rcTvSeries.setLayoutManager(new LinearLayoutManager(getContext()));

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        showLoading(true);

        requestData(Objects.requireNonNull(getActivity()));

        tvListAdapter = new TvSeriesListAdapter(listTvDb);
        tvListAdapter.addContext(getContext());
        rcTvSeries.setAdapter(tvListAdapter);

        showLoading(false);

    }

    private void requestData(FragmentActivity activity) {
        try (Cursor query = activity.getContentResolver().query(TvMappingHelper.URI_CONTENT_TV, null, null, null, null)) {
            listTvDb.addAll(TvMappingHelper.mapCursorToArrayList(query));
        }
    }

    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
            rcTvSeries.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            rcTvSeries.setVisibility(View.VISIBLE);
        }
    }

}
