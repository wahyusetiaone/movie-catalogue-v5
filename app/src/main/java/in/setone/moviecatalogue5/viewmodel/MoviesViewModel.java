package in.setone.moviecatalogue5.viewmodel;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;
import in.setone.moviecatalogue5.BuildConfig;
import in.setone.moviecatalogue5.model.MoviesModel;
import in.setone.moviecatalogue5.model.MoviesResponseModel;
import in.setone.moviecatalogue5.parcelable.Movies;
import in.setone.moviecatalogue5.repository.MoviesRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

//TODO: Learn DataBinding

public class MoviesViewModel extends ViewModel {

    private MutableLiveData<MoviesResponseModel> mutableLiveData;
    private LiveData<List<Movies>> liveData;
    private MoviesRepository moviesRepository;
    private final String API_KEY = BuildConfig.TMDB_API_KEY;
    private final String EN_LANGUANGE = "en-US";
    private final String ID_LANGUANGE = "id-ID";

    public void init(Activity activity, Boolean reload) {
        if (!reload){
            if (mutableLiveData != null) {
                return;
            }
        }
        moviesRepository = MoviesRepository.getInstance(activity);

        String lang = Locale.getDefault().getDisplayLanguage();

        if (lang.equals("Bahasa Indonesia")) {
            mutableLiveData = moviesRepository.getMovies(API_KEY, ID_LANGUANGE, activity);
        } else {
            mutableLiveData = moviesRepository.getMovies(API_KEY, EN_LANGUANGE, activity);
        }

        //call from database
        liveData = moviesRepository.read();

//        log movies
//        test.observe(activity, p -> {
//            assert p != null;
//            for (MoviesModel tableModel : p) {
//                Log.wtf("DATA", String.valueOf(tableModel.getId()));
//            }
//        });

    }

    public LiveData<List<Movies>> getMoviesDatabase() {
        return liveData;
    }

    public LiveData<MoviesResponseModel> getMoviesApi() {
        return mutableLiveData;
    }

    public final void clear(ArrayList<Movies> listMovies) {
        listMovies.clear();
        mutableLiveData.postValue(null);
    }

    public final void insert(MoviesModel movie, Activity activity) {
        moviesRepository = MoviesRepository.getInstance(activity);
        try {
            moviesRepository.insert(movie);
        } catch (Exception ee) {
            Log.wtf("ERROR INSERT THE MOVIE", ee.toString());
        }
    }

    public final void delete(MoviesModel movie, Activity activity) {
        moviesRepository = MoviesRepository.getInstance(activity);
        try {
            moviesRepository.delete(movie);
        } catch (Exception ee) {
            Log.wtf("ERROR DELETE THE MOVIE", ee.toString());
        }
    }

    public LiveData<MoviesResponseModel> searchMoviesApi(String query, Activity activity) {
        moviesRepository = MoviesRepository.getInstance(activity);

        String lang = Locale.getDefault().getDisplayLanguage();

        if (lang.equals("Bahasa Indonesia")) {
            mutableLiveData = moviesRepository.searchMovie(API_KEY, ID_LANGUANGE, query, activity);
        } else {
            mutableLiveData = moviesRepository.searchMovie(API_KEY, EN_LANGUANGE, query, activity);
        }
        return mutableLiveData;
    }

}
