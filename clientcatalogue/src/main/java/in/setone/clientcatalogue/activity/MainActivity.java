package in.setone.clientcatalogue.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;
import in.setone.clientcatalogue.R;
import in.setone.clientcatalogue.fragment.MovieFragment;
import in.setone.clientcatalogue.fragment.TvSeriesFragment;

public class MainActivity extends AppCompatActivity {

    private FragmentTransaction ft;
    private final String MY_FRAGMENT_TAG = "CURRENT_FRAGMENT";
    private final String KEY_LOCALE = "LOCALE";

    private BottomNavigationView navigation;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_movies:
                    ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frameLayout, new MovieFragment(), MY_FRAGMENT_TAG);
                    ft.commit();
                    return true;
                case R.id.navigation_tvseries:
                    ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frameLayout, new TvSeriesFragment(), MY_FRAGMENT_TAG);
                    ft.commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if (savedInstanceState == null) {
            navigation.setSelectedItemId(R.id.navigation_movies);
        }
    }

}
