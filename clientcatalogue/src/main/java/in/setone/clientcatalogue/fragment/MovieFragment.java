package in.setone.clientcatalogue.fragment;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ProgressBar;
import in.setone.clientcatalogue.R;
import in.setone.clientcatalogue.adapter.MoviesListAdapter;
import in.setone.clientcatalogue.helper.MovieMappingHelper;
import in.setone.clientcatalogue.model.MovieModel;
import in.setone.clientcatalogue.parceable.MovieParcel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MovieFragment extends Fragment {

    private RecyclerView rcMovies;
    private List<MovieParcel> listMoviesDb = new ArrayList<>();
    private MoviesListAdapter moviesListAdapter;
    private ProgressBar progressBar;

    public MovieFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_movie, container, false);
        progressBar = rootView.findViewById(R.id.progressBarMovies);
        rcMovies = rootView.findViewById(R.id.rvMovie);

        rcMovies.setHasFixedSize(true);
        rcMovies.setLayoutManager(new LinearLayoutManager(getContext()));
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showLoading(true);

        requestData(Objects.requireNonNull(getActivity()));

        moviesListAdapter = new MoviesListAdapter(listMoviesDb);
        moviesListAdapter.addContext(getContext());
        rcMovies.setAdapter(moviesListAdapter);

        showLoading(false);
    }

    private void requestData(FragmentActivity activity) {
        try (Cursor query = activity.getContentResolver().query(MovieMappingHelper.URI_CONTENT_MOVIE, null, null, null, null)) {
            listMoviesDb.addAll(MovieMappingHelper.mapCursorToArrayList(query));
        }
        showLoading(false);
    }

    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
            rcMovies.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            rcMovies.setVisibility(View.VISIBLE);
        }
    }

}
