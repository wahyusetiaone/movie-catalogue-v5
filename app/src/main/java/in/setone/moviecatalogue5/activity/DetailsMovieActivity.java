package in.setone.moviecatalogue5.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import in.setone.moviecatalogue5.R;
import in.setone.moviecatalogue5.adapter.MoviesListAdapter;
import in.setone.moviecatalogue5.parcelable.Movies;
import in.setone.moviecatalogue5.viewmodel.MoviesViewModel;
import in.setone.moviecatalogue5.widget.StackWidgetService;

public class DetailsMovieActivity extends AppCompatActivity {

    private Movies data = new Movies();
    private static boolean fav;

    private TextView tvTitle,tvDetails,tvLabel;
    private ImageView ivMovies,ivFav;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_movie);

        customActionBar();

        initializeComponent();

        showLoading(true);

        Intent intent = getIntent();
        data = intent.getParcelableExtra(MoviesListAdapter.KEY_OF_MOVIE);
        fav = intent.getBooleanExtra(MoviesListAdapter.KEY_OF_MOVIE_FAVORITE,false);

        setData();

        favAction();
    }


    private void favAction() {

        MoviesViewModel viewModel = new MoviesViewModel();

        changeFavImage();

        ivFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fav){
                    viewModel.delete(data,DetailsMovieActivity.this);
                    fav =false;
                    changeFavImage();
                }else {
                    viewModel.insert(data,DetailsMovieActivity.this);
                    fav=true;
                    changeFavImage();
                }
                StackWidgetService.updateWidget(getBaseContext(),getApplication());
            }
        });
    }

    private void changeFavImage() {
        if (fav){
            ivFav.setImageResource(R.drawable.ic_star_black_24dp);
        }else {
            ivFav.setImageResource(R.drawable.ic_star_border_black_24dp);
        }
    }

    private void customActionBar() {
        this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar);
        //getSupportActionBar().setElevation(0);
        View view = getSupportActionBar().getCustomView();
        TextView name = view.findViewById(R.id.tvActionBar);
        name.setText(getResources().getString(R.string.name_custom_actionbar_movies));
        ImageView imageView = view.findViewById(R.id.ivBackActionBar);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void setData() {

        tvTitle.setText(data.getTitle());
        if (data.getOverview().equals("")){
            tvDetails.setText(R.string.no_detalis);
        }else {
            tvDetails.setText(data.getOverview());
        }

        final String url_image = "https://image.tmdb.org/t/p/w185" + data.getPoster_path();
        Picasso.get().load(url_image).into(ivMovies);

        //dummy load
        new CountDownTimer(3000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                showLoading(false);
            }
        }.start();

    }

    private void initializeComponent() {

        tvDetails = (TextView) findViewById(R.id.tvMoviesDetalis);
        tvLabel = (TextView) findViewById(R.id.tvMDetais);
        tvTitle = (TextView) findViewById(R.id.tvMoviesTitle);
        progressBar = (ProgressBar) findViewById(R.id.progressBarMDetails);
        ivMovies = (ImageView) findViewById(R.id.ivImgMovies);
        ivFav = (ImageView) findViewById(R.id.ivFavMovie);

    }

    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
            tvDetails.setVisibility(View.GONE);
            tvLabel.setVisibility(View.GONE);
            tvTitle.setVisibility(View.GONE);
            ivMovies.setVisibility(View.GONE);
            ivFav.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            tvDetails.setVisibility(View.VISIBLE);
            tvLabel.setVisibility(View.VISIBLE);
            tvTitle.setVisibility(View.VISIBLE);
            ivMovies.setVisibility(View.VISIBLE);
            ivFav.setVisibility(View.VISIBLE);
        }
    }

}
