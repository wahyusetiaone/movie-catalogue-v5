package in.setone.clientcatalogue.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import in.setone.clientcatalogue.R;
import in.setone.clientcatalogue.activity.DetailsMovieActivity;
import in.setone.clientcatalogue.parceable.MovieParcel;

import java.util.List;

public class MoviesListAdapter extends RecyclerView.Adapter<MoviesListAdapter.ListViewHolder> {

    private List<MovieParcel> arrayMovies;
    private Context context;

    public static String KEY_OF_MOVIE = "DATA_MOVIE";

    public MoviesListAdapter(List<MovieParcel> moviesArrayList){
        this.arrayMovies = moviesArrayList;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row_movies, viewGroup, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder listViewHolder, int i) {
        final MovieParcel movies = arrayMovies.get(i);

        String url_image = "https://image.tmdb.org/t/p/w185" + movies.getPoster_path();
        Picasso.get().load(url_image).into(listViewHolder.imgPoster);

        listViewHolder.tvTitle.setText(movies.getTitle());

        listViewHolder.tvDetail.setText(movies.getOverview());

        if (movies.getOverview().equals("")){
            listViewHolder.tvDetail.setText(R.string.no_details);
        }

        listViewHolder.cVMovies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, DetailsMovieActivity.class);
                i.putExtra(KEY_OF_MOVIE, movies);
                context.startActivity(i);
            }
        });


    }

    @Override
    public int getItemCount() {
        return arrayMovies.size();
    }

    public void addContext(Context context) {
        this.context = context;
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {

        ImageView imgPoster;
        TextView tvTitle,tvDetail;
        CardView cVMovies;

        public ListViewHolder(@NonNull View itemView) {
            super(itemView);

            imgPoster = itemView.findViewById(R.id.imgPosterMovie);
            tvTitle = itemView.findViewById(R.id.tvTitleMovie);
            tvDetail = itemView.findViewById(R.id.tvDetailMovie);
            cVMovies = itemView.findViewById(R.id.cVMovies);
        }
    }
}
