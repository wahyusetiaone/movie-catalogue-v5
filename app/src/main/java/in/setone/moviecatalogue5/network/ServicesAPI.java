package in.setone.moviecatalogue5.network;

import in.setone.moviecatalogue5.model.MoviesResponseModel;
import in.setone.moviecatalogue5.model.TvResponseModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ServicesAPI {

    @GET("discover/movie")
    Call<MoviesResponseModel> getDataMovies(
            @Query("api_key") String apiKey,
            @Query("language") String language
    );

    @GET("discover/tv")
    Call<TvResponseModel> getDataTv(
            @Query("api_key") String apiKey,
            @Query("language") String language
    );

    @GET("search/movie")
    Call<MoviesResponseModel> searchMovie(
            @Query("api_key") String apiKey,
            @Query("language") String language,
            @Query("query") String query
    );

    @GET("search/tv")
    Call<TvResponseModel> searchTv(
            @Query("api_key") String apiKey,
            @Query("language") String language,
            @Query("query") String query
    );

    @GET("discover/movie")
    Call<MoviesResponseModel> getReleaseMovies(
            @Query("api_key") String apiKey,
            @Query("primary_release_date.gte") String gte,
            @Query("primary_release_date.lte") String lte
    );

}
