package in.setone.moviecatalogue5.room;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.*;
import android.database.Cursor;
import in.setone.moviecatalogue5.model.TvModel;
import in.setone.moviecatalogue5.parcelable.TvSeries;

import java.util.List;

@Dao
public interface TvDao {

    @Insert (onConflict = OnConflictStrategy.IGNORE)
    void insert(TvModel moviesModel);

    @Query("DELETE FROM tv_fav")
    void deleteAll();

    @Delete
    void delete(TvModel tvModel);

    @Query("SELECT * from tv_fav ORDER BY id ASC")
    LiveData<List<TvSeries>> getResults();

    //for contentprovider
    @Query("SELECT * from tv_fav ORDER BY id ASC")
    Cursor getCursorTv();

    @Query("SELECT * from tv_fav WHERE id = :id")
    Cursor loadCursorTv(long id);

}
