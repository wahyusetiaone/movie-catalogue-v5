package in.setone.moviecatalogue5.contentprovider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import in.setone.moviecatalogue5.room.AppDatabase;
import in.setone.moviecatalogue5.room.MoviesDao;
import in.setone.moviecatalogue5.room.TvDao;

//TODO::URI NOT MATCHER
public class AppContentProvider extends ContentProvider {

    // Use an int for each URI we will run, this represents the different queries
    private static final int MOVIE = 100;
    private static final int MOVIE_ID = 101;
    private static final int TV = 200;
    private static final int TV_ID = 201;

    private static final String PATH_MOVIE = "movies";
    private static final String PATH_TV = "tv";

    private static final String AUTHORITY = "in.setone.moviecatalogue5";
//    private static final Uri CONTENT = Uri.parse("content://"+AUTH);
        private static final UriMatcher sUriMatcher = buildUriMatcher();
    private AppDatabase appDatabase;
    private  MoviesDao moviesDao;
    private TvDao tvDao;
    private long _id;

    /**
     * Builds a UriMatcher that is used to determine witch database request is being made.
     */
    private static UriMatcher buildUriMatcher(){

        // All paths to the UriMatcher have a corresponding code to return
        // when a match is found (the ints above).
        UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(AUTHORITY, PATH_MOVIE, MOVIE);
        matcher.addURI(AUTHORITY, PATH_MOVIE + "/#", MOVIE_ID);
        matcher.addURI(AUTHORITY, PATH_TV, TV);
        matcher.addURI(AUTHORITY, PATH_TV + "/#", TV_ID);

        return matcher;
    }

//    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
//    static {
//        // content://com.dicoding.picodiploma.mynotesapp/note
//        sUriMatcher.addURI(auth, TABLE_NAME, MOVIE);
//        // content://com.dicoding.picodiploma.mynotesapp/note/id
//        sUriMatcher.addURI(auth,TABLE_NAME + "/#", MOVIE_ID);
//    }

    public AppContentProvider() {
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Implement this to handle requests to delete one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public String getType(Uri uri) {
        // TODO: Implement this to handle requests for the MIME type of the data
        // at the given URI.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        // TODO: Implement this to handle requests to insert a new row.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public boolean onCreate() {
        appDatabase = AppDatabase.getDatabase(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        Cursor retCursor;
        switch(sUriMatcher.match(uri)){
            case MOVIE:
                appDatabase.getOpenHelper().getWritableDatabase();
                moviesDao = appDatabase.moviesDao();
                retCursor = moviesDao.getCursorMovies();
                break;
            case MOVIE_ID:
                _id = ContentUris.parseId(uri);
                appDatabase.getOpenHelper().getWritableDatabase();
                moviesDao = appDatabase.moviesDao();
                retCursor = moviesDao.loadCursorMovie(_id);
                break;
            case TV:
                appDatabase.getOpenHelper().getWritableDatabase();
                tvDao = appDatabase.tvDao();
                retCursor = tvDao.getCursorTv();
                break;
            case TV_ID:
                _id = ContentUris.parseId(uri);
                appDatabase.getOpenHelper().getWritableDatabase();
                tvDao = appDatabase.tvDao();
                retCursor = tvDao.loadCursorTv(_id);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        // Set the notification URI for the cursor to the one passed into the function. This
        // causes the cursor to register a content observer to watch for changes that happen to
        // this URI and any of it's descendants. By descendants, we mean any URI that begins
        // with this path.
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        // TODO: Implement this to handle requests to update one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
