package in.setone.moviecatalogue5.parcelable;

import android.os.Parcel;
import android.os.Parcelable;
import in.setone.moviecatalogue5.model.MoviesModel;

public class Movies extends MoviesModel implements Parcelable {

    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeDouble(this.popularity);
        dest.writeInt(this.vote_count);
        dest.writeByte((byte) (this.video ? 1: 0));
        dest.writeString(this.poster_path);
        dest.writeByte((byte) (this.adult ? 1: 0));
        dest.writeString(this.backdrop_path);
        dest.writeString(this.original_language);
        dest.writeString(this.original_title);
        dest.writeDouble(this.vote_average);
        dest.writeString(this.overview);
        dest.writeString(this.release_date);

    }
    public Movies() {
    }
    private Movies(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.popularity = in.readDouble();
        this.vote_count = in.readInt();
        this.video = in.readByte() != 0;
        this.poster_path = in.readString();
        this.adult = in.readByte() != 0;;
        this.backdrop_path = in.readString();
        this.original_language = in.readString();
        this.original_title = in.readString();
        this.vote_average = in.readDouble();
        this.overview = in.readString();
        this.release_date = in.readString();
    }
    public static final Creator<Movies> CREATOR = new Creator<Movies>() {
        @Override
        public Movies createFromParcel(Parcel source) {
            return new Movies(source);
        }
        @Override
        public Movies[] newArray(int size) {
            return new Movies[size];
        }
    };
}
