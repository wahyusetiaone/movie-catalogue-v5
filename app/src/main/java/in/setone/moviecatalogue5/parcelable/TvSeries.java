package in.setone.moviecatalogue5.parcelable;

import android.os.Parcel;
import android.os.Parcelable;
import in.setone.moviecatalogue5.model.TvModel;

public class TvSeries extends TvModel implements Parcelable {

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.original_name);
        dest.writeString(this.name);
        dest.writeDouble(this.popularity);
        dest.writeInt(this.vote_count);
        dest.writeString(this.first_air_date);
        dest.writeString(this.backdrop_path);
        dest.writeString(this.original_language);
        dest.writeDouble(this.vote_average);
        dest.writeString(this.overview);
        dest.writeString(this.poster_path);
    }

    public TvSeries(){}

    private TvSeries(Parcel in) {
        this.id = in.readInt();
        this.original_name = in.readString();
        this.name = in.readString();
        this.popularity = in.readDouble();
        this.vote_count = in.readInt();
        this.first_air_date = in.readString();
        this.backdrop_path = in.readString();
        this.original_language = in.readString();
        this.vote_average = in.readDouble();
        this.overview = in.readString();
        this.poster_path = in.readString();
    }

    public static final Creator<TvSeries> CREATOR = new Creator<TvSeries>() {
        @Override
        public TvSeries createFromParcel(Parcel source) {
            return new TvSeries(source);
        }
        @Override
        public TvSeries[] newArray(int size) {
            return new TvSeries[size];
        }
    };
}
