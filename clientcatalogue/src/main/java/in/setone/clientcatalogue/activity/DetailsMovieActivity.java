package in.setone.clientcatalogue.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import in.setone.clientcatalogue.R;
import in.setone.clientcatalogue.R.string;
import in.setone.clientcatalogue.adapter.MoviesListAdapter;
import in.setone.clientcatalogue.model.MovieModel;
import in.setone.clientcatalogue.parceable.MovieParcel;

public class DetailsMovieActivity extends AppCompatActivity {

    private MovieParcel data = new MovieParcel();

    private TextView tvTitle,tvDetails,tvLabel;
    private ImageView ivMovies;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_movie);

        customActionBar();

        initializeComponent();

        showLoading(true);

        Intent intent = getIntent();
        data = intent.getParcelableExtra(MoviesListAdapter.KEY_OF_MOVIE);

        setData();

    }

    private void customActionBar() {
        this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar);
        //getSupportActionBar().setElevation(0);
        View view = getSupportActionBar().getCustomView();
        TextView name = view.findViewById(R.id.tvActionBar);
        name.setText(R.string.details_movie);
        ImageView imageView = view.findViewById(R.id.ivBackActionBar);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void setData() {

        tvTitle.setText(data.getTitle());
        if (data.getOverview() == null || data.getOverview().equals("")){
            tvDetails.setText(R.string.no_details);
        }else {
            tvDetails.setText(data.getOverview());
        }

        final String url_image = "https://image.tmdb.org/t/p/w185" + data.getPoster_path();
        Picasso.get().load(url_image).into(ivMovies);

        //dummy load
        new CountDownTimer(3000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                showLoading(false);
            }
        }.start();

    }

    private void initializeComponent() {

        tvDetails = (TextView) findViewById(R.id.tvMoviesDetalis);
        tvLabel = (TextView) findViewById(R.id.tvMDetais);
        tvTitle = (TextView) findViewById(R.id.tvMoviesTitle);
        progressBar = (ProgressBar) findViewById(R.id.progressBarMDetails);
        ivMovies = (ImageView) findViewById(R.id.ivImgMovies);

    }

    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
            tvDetails.setVisibility(View.GONE);
            tvLabel.setVisibility(View.GONE);
            tvTitle.setVisibility(View.GONE);
            ivMovies.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            tvDetails.setVisibility(View.VISIBLE);
            tvLabel.setVisibility(View.VISIBLE);
            tvTitle.setVisibility(View.VISIBLE);
            ivMovies.setVisibility(View.VISIBLE);
        }
    }

}
