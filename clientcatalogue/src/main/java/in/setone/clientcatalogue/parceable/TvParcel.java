package in.setone.clientcatalogue.parceable;

import android.os.Parcel;
import android.os.Parcelable;
import in.setone.clientcatalogue.model.TvModel;

public class TvParcel extends TvModel implements Parcelable {

    public TvParcel(){}

    public TvParcel(int id, String original_name, String name, double popularity, int vote_count, String first_air_date, String backdrop_path, String original_language, double vote_average, String overview, String poster_path) {
        super(id, original_name, name, popularity, vote_count, first_air_date, backdrop_path, original_language, vote_average, overview, poster_path);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.original_name);
        dest.writeString(this.name);
        dest.writeDouble(this.popularity);
        dest.writeInt(this.vote_count);
        dest.writeString(this.first_air_date);
        dest.writeString(this.backdrop_path);
        dest.writeString(this.original_language);
        dest.writeDouble(this.vote_average);
        dest.writeString(this.overview);
        dest.writeString(this.poster_path);
    }

    private TvParcel(Parcel in) {
        this.id = in.readInt();
        this.original_name = in.readString();
        this.name = in.readString();
        this.popularity = in.readDouble();
        this.vote_count = in.readInt();
        this.first_air_date = in.readString();
        this.backdrop_path = in.readString();
        this.original_language = in.readString();
        this.vote_average = in.readDouble();
        this.overview = in.readString();
        this.poster_path = in.readString();
    }

    public static final Parcelable.Creator<TvParcel> CREATOR = new Parcelable.Creator<TvParcel>() {
        @Override
        public TvParcel createFromParcel(Parcel source) {
            return new TvParcel(source);
        }
        @Override
        public TvParcel[] newArray(int size) {
            return new TvParcel[size];
        }
    };
}
