package in.setone.moviecatalogue5.repository;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import in.setone.moviecatalogue5.model.AppSettingModel;
import in.setone.moviecatalogue5.room.AppDatabase;
import in.setone.moviecatalogue5.room.SettingDao;

public class SettingRepository {
    private static SettingRepository settingRepository;

    private SettingDao settingDao;

    public static SettingRepository getInstance(Activity activity) {
        if (settingRepository == null) {
            settingRepository = new SettingRepository(activity);
        }
        return settingRepository;
    }

    private AppDatabase appDatabase;

    public SettingRepository(Activity activity) {
        appDatabase = AppDatabase.getDatabase(activity);
        appDatabase.getOpenHelper().getWritableDatabase();
        settingDao = appDatabase.settingDao();
    }



    public LiveData<AppSettingModel> read() {

        return settingDao.getSetting();

    }

    public void update(AppSettingModel appSettingModel) {
        new insertAsyncTask(settingDao).execute(appSettingModel);
    }

    private static class insertAsyncTask extends AsyncTask<AppSettingModel, Void, Void> {

        private SettingDao mAsyncTaskDao;

        insertAsyncTask(SettingDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final AppSettingModel... params) {
            mAsyncTaskDao.update(params[0]);
            return null;
        }
    }

}
