package in.setone.moviecatalogue5.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "tv_fav")
public class TvModel {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    @SerializedName("id")
    @Expose
    protected int id;

    @ColumnInfo(name = "original_name")
    @SerializedName("original_name")
    @Expose
    protected String original_name;

    @ColumnInfo(name = "name")
    @SerializedName("name")
    @Expose
    protected String name;

    @ColumnInfo(name = "popularity")
    @SerializedName("popularity")
    @Expose
    protected double popularity;

    @ColumnInfo(name = "vote_count")
    @SerializedName("vote_count")
    @Expose
    protected int vote_count;

    @ColumnInfo(name = "first_air_date")
    @SerializedName("first_air_date")
    @Expose
    protected String first_air_date;

    @ColumnInfo(name = "backdrop_path")
    @SerializedName("backdrop_path")
    @Expose
    protected String backdrop_path;

    @ColumnInfo(name = "original_language")
    @SerializedName("original_language")
    @Expose
    protected String original_language;

    @ColumnInfo(name = "vote_average")
    @SerializedName("vote_average")
    @Expose
    protected double vote_average;

    @ColumnInfo(name = "overview")
    @SerializedName("overview")
    @Expose
    protected String overview;

    @ColumnInfo(name = "poster_path")
    @SerializedName("poster_path")
    @Expose
    protected String poster_path;

    public String getOriginal_name() {
        return original_name;
    }

    public void setOriginal_name(String original_name) {
        this.original_name = original_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPopularity() {
        return popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    public int getVote_count() {
        return vote_count;
    }

    public void setVote_count(int vote_count) {
        this.vote_count = vote_count;
    }

    public String getFirst_air_date() {
        return first_air_date;
    }

    public void setFirst_air_date(String first_air_date) {
        this.first_air_date = first_air_date;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public void setBackdrop_path(String backdrop_path) {
        this.backdrop_path = backdrop_path;
    }

    public String getOriginal_language() {
        return original_language;
    }

    public void setOriginal_language(String original_language) {
        this.original_language = original_language;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getVote_average() {
        return vote_average;
    }

    public void setVote_average(double vote_average) {
        this.vote_average = vote_average;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }
}
