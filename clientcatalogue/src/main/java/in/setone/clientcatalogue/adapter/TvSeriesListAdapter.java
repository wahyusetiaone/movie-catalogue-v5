package in.setone.clientcatalogue.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import in.setone.clientcatalogue.R;
import in.setone.clientcatalogue.activity.DetailsTvSeriesActivity;
import in.setone.clientcatalogue.parceable.TvParcel;

import java.util.List;

public class TvSeriesListAdapter extends RecyclerView.Adapter<TvSeriesListAdapter.ListViewHolder> {

    private List<TvParcel> arrayTvSeries;
    private Context context;

    public static String KEY_OF_TVSERIES = "DATA_TV";

    public TvSeriesListAdapter(){}

    public TvSeriesListAdapter(List<TvParcel> tvSeriesArrayList){
        this.arrayTvSeries = tvSeriesArrayList;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row_tvseries, viewGroup, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder listViewHolder, int i) {
        final TvParcel tvSeries = arrayTvSeries.get(i);
        boolean fav;

        String url_image = "https://image.tmdb.org/t/p/w185" + tvSeries.getPoster_path();
        Picasso.get().load(url_image).into(listViewHolder.imgPoster);

        listViewHolder.tvTitle.setText(tvSeries.getName());

        listViewHolder.tvDetail.setText(tvSeries.getOverview());

        if (tvSeries.getOverview().equals("")){
            listViewHolder.tvDetail.setText(R.string.no_details);
        }

        listViewHolder.cVTvSeries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, DetailsTvSeriesActivity.class);
                i.putExtra(KEY_OF_TVSERIES, tvSeries);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayTvSeries.size();
    }

    public void addContext(Context context) {
        this.context = context;
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {

        ImageView imgPoster;
        TextView tvTitle,tvDetail;
        CardView cVTvSeries;

        public ListViewHolder(@NonNull View itemView) {
            super(itemView);

            imgPoster = itemView.findViewById(R.id.imgPosterTvSeries);
            tvTitle = itemView.findViewById(R.id.tvTitleTvSeries);
            tvDetail = itemView.findViewById(R.id.tvDetailTvSeries);
            cVTvSeries = itemView.findViewById(R.id.cVTV);
        }
    }
}
